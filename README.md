CAMD manual
===========

Welcome to the CAMD manual. Here you should be able to find all information that you need for working at camd.

You can speak with us at

    https://camd.zulipchat.com

A lot of nice links can be found at

    https://wiki.fysik.dtu.dk/


If you are going to develop on GPAW or ASE
==========================================
First login to the cluster using

    `ssh USERNAME@sylg`

Then download and install ASE and GPAW as described in

    https://wiki.fysik.dtu.dk/gpaw/platforms/Linux/Niflheim/Niflheim.html

CAMD Toolkit
============
These are the tools and packages that you need to get familiar with when working at CAMD.

- [GPAW](wiki.fysik.dtu.dk/gpaw)
- [ASE](wiki.fysik.dtu.dk/ase)
- [MyQueue](myqueue.readthedocs.io)

If you doing workflow related tasks then take a look at

- [ASR](asr.readthedocs.io)

Resources
=========

Notes on the PAW formalism

    https://wiki.fysik.dtu.dk/gpaw/documentation/literature.html


Setup your local desktop
========================

Ask niflheim-support@fysik.dtu.dk to:

- Get access to /home/niflheim/$USER and /home/scratch/$USER

Change the default installation path for python packages such that they won't
fill up your disk quota: 

```console
$ echo "export PYTHONUSERBASE=/scratch/$USER/.local" >> .bashrc
```

Choosing a good editor
----------------------
It can be very convenient to set up the Compile your own version of emacs.

- Download the latest version of emacs.
- Get a superuser to run yum-builddep emacs on your computer
- Follow the instructions in the emacs folder to install

It is recommended to install the elpy package which makes python programming in 
emacs much easier.

Starting new project and making project specific virtual environment
====================================================================

When starting a new project it is very useful to make an isolate code
environment known as a virtual environment (in short `venv`), to help you stay
in control of what code submitted jobs are using. See the link in the bottom of
this section for the concrete details on how to set up project specific `venv`.
In practice, the isolated environment is practice simply a folder containing a
special `activate` file that when sourced primarily takes care of making python
use only the packages installed in the `venv`. The `venv` is automatically
activated by MyQueue when a job is submitted. If you want to use the environment
interactively, then you have to activate it yourself.

[Find all the details and follow the instructions
here](<https://wiki.fysik.dtu.dk/gpaw/platforms/Linux/Niflheim/build.html#creating-the-venv>).

Cron
============
Niflheim has Cron installed. Cron is a job scheduler (https://en.wikipedia.org/wiki/Cron). It allows you to specify arbitrary commands to be run at arbitrary times or intervals. This is useful for workflow management over a vacation, for example.

To set it up log in to Niflheim and run "crontab -e". This will open your personal crontab-file in a vim editor. In this file you add the jobs you want to run along with their schedule.

Note that when running a job with Cron on Niflheim it seems you need to source your bashrc and activate your venv, see examples below.

Cron syntax
----------------------
This is just the basics; for a more detailed walkthrough you can google it or check wikipedia. Each line of the crontab-file represents one job. The first part of a job consists of the scheduling instructions and the last part is the command to run. The format is

`minute hour day-of-month month day-of-week command`

The first five fields thus specify the schedule and the last field is the command. Times are specified with integers, *, or interval-notation. Interval-notation is `*/N` where `N` is the interval. For example, if `minute=*/15`, then that means "every 15 minutes" (but not necessarily every 15 minutes every day - that depends on the other scheduling parameters).

Cron syntax examples
----------------------
Here are some examples of job specifications in english translated to cron.

English: "Run `mq workflow agreatworkflow.py ~/myinterestingmaterials/*/*/*/` once a day at 11 am"

Cron: `0 11 * * * (source ~/.bashrc; source <path to my venv>/bin/activate; mq workflow agreatworkflow.py ~/myinterestingmaterials/*/*/*/)`

<br>
English: "Run `mq workflow adecentworkflow.py ~/4dmaterials/*/` every third day at 5 pm"

Cron: `0 17 */3 * * (source ~/.bashrc; source <path to my venv>/bin/activate; mq workflow adecentworkflow.py ~/4dmaterials/*/)`

Note that you need to source bashrc and your venv-activate before running a command.

Caution
----------------------
If you set up an automated submission of jobs, beware that it may try to submit a lot of tasks. You should put in some way of limiting the total amount of jobs that can be submitted, so you don't reach your diskspace limit or blow up Niflheim in some other way.


Example: Automated Workflow with Job limit
----------------------
Here is a larger example that runs a workflow command every day at noon but only if there are not too many running or queued jobs already. We also added a flag to ignore warnings from python so that cron will not send too many emails.

Crontab: 0 12 * * * source ~/submitjobs.sh >> ~/cron.out

In ~/submitjobs.sh:

```
#!bin/bash

source ~/.bashrc
source <path to venv>/bin/activate

if [ `mq ls -sqr <path to root of job-tree> | wc -l` -lt <job limit> ]
then
    python3 -W ignore -m myqueue workflow <path to workflow script> <path to folders to submit to>
fi
```
